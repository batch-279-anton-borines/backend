// User "require" directive to load node.js modules
// A "http module" lets node.js transfer data using the hyper text transfer protocol (http)
// Clients/(browers) and servers (node JS/express JS applications) communicate by exchanging individual messages.

let http = require("http");

// Using this module's createServer method, we can create an HTTP server that listens to requests on specified ports

// A port is a virtual point where ntwork connections start and end
// Each port is associated with a specifid process or service
// The server will be assigned to port 4000

http.createServer(function (request, response){
	// Use writeHead() method to:
	// --> set a status code for the message --> 200 means ok
	// --> sets the content type of the response as a plain text
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello, World!");
}).listen(4000);

console.log("Server running at localhost:4000");