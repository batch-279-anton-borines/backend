const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
		userId : {
			type : String,
			required : true
		},
		productId : {
			type : String,
			required : [true, "Product ID is required"]
		},
		productName : {
			type : String,
			required : true
		},
		quantity : {
			type : Number,
			required : [true, "Order quantity is required"]
		},
		price : {
			type : Number,
			required : true
		},
		amountDue : {
			type : Number,
			required : true
		},
		orderedOn : {
			type : Date,
			default : new Date()
		}
})

module.exports = mongoose.model("Order", orderSchema);