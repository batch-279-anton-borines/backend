const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No. is required"]
	},
	address : [{
		streetAddress : {
			type : String,
			required : [true, "Street Address is required"]
		},
		city : {
			type : String,
			required : [true, "City or municipality is required"]
		},
		province : {
			type : String,
			required : [true, "Province is required"]
		},
		country : {
			type : String,
			default : "PH"
		}
	}],
	isFemale : {
		type : Boolean,
		required : [true, "Biological sex is required"]
	},
	birthYear : {
		type : Number,
		required : [true, "Birth year is required"]
	} 
})

module.exports = mongoose.model("User", userSchema);
