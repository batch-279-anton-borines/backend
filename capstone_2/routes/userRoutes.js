const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.get("/:userId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(req.params, userData).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all users
router.get("/all", (req,res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController)); 
});

// Enroll user to a course
// Discussion Code
/*router.post("/enroll", (req,res)=>{
	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
*/

// Activity Code
/*router.post("/enroll", auth.verify, (req,res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}
	userController.enroll(data, isAdmin).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in our "index.js" file

*/

module.exports = router;