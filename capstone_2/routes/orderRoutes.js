const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");

// Create a new order
router.post('/:productId/checkout', auth.verify, (req,res)=>{
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		quantity : req.body
	}
	orderController.checkout(req.params,data).then(resultFromController => res.send(resultFromController))
})

// // Get all orders
router.get("/all", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
})
// // Update order status
// router.put('/orders/:orderId/status', orderController.updateOrderStatus);

module.exports = router;