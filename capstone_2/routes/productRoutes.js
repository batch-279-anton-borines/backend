const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Get all products
router.get("/all", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.getAll(data).then(resultFromController => res.send(resultFromController));
})

// Get all available 
router.get("/", (req, res) => {
	productController.getAvailable().then(resultFromController => res.send(resultFromController));
})

// Get a specific product
router.get("/:productId", (req, res) => {
	console.log(req.body.name);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update a product
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// archive a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

// activate a product
router.put("/:productId/activate", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});


// // Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;