const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin,
		mobileNo : reqBody.mobileNo,
		address : reqBody.address,
		isFemale : reqBody.isFemale,
		birthYear : reqBody.birthYear
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} 
		else {
			return true;
		};
	});
};

// User authentication
module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} 
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }
			} else {
				return false;
			};
		};
	});
};

// Retrieve user details (user only or admin)
module.exports.getProfile = (reqParams, userData) => {
	return User.findById(reqParams.userId).then(result =>{
		console.log(reqParams.userId);
		if(result == null){
			return false;
		}
		else if(reqParams.userId == userData.id || userData.isAdmin) {
			result.password = "";
			return result;
		}
		else {
			return "Mind your own business";
		}
	})
} 

// Retrieve all users 
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};