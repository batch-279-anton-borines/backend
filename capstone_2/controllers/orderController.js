const Product = require('../models/Product');
const User = require('../models/User')
const Order = require('../models/Order');
const auth = require("../auth");

// Checkout a product 
module.exports.checkout = async (reqParams,data)=>{
	if(data.isAdmin){
		return false;
	}
	else {
		return Product.findById(reqParams.productId).then(product =>{
			let newOrder = new Order ({
				
					userId : data.userId,
					productId : product._id,
					productName : product.name,
					price : product.price,
					quantity : data.quantity.quantity,
					amountDue : Math.round(product.price * data.quantity.quantity,2)
				})
			
			return newOrder.save().then((order,err)=>{
				if(err){
					return err;
				}
				else {
					return order;
				}
			})

			})
	}
}

module.exports.getAllOrders = (data) => {
	if(data.isAdmin){
		return Order.find({}).then(result => {
			return result;
		})		
	}
	else {
		return "Only admins can do this"
	}
}