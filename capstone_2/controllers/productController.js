const Product = require("../models/Product");


// Create a new product
module.exports.addProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			isAvailable: data.product.isAvailable,
			price: data.product.price
		})
		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 
	let message = Promise.resolve("Only admins can do this");
	return message.then((value) => {
		return value;
	})
}

// get all products
module.exports.getAll = (data) => {
	if(data.isAdmin){
		return Product.find({}).then(result => {
			return result;
		})		
	}
	else {
		return Product.find({isAvailable : true}).then(result => {
			return result;
		})
	}
}

// get all available products
module.exports.getAvailable = () => {
		return Product.find({isAvailable : true}).then(result => {
			return result;
		})
}

// get a specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// Update a product
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(`Updater is admin? ${isAdmin}`);
	if(isAdmin){
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			isAvailable : reqBody.isAvailable,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return updatedProduct;
			}
		})
	}

	let message = Promise.resolve("Only admins can do this");
	return message.then((value) => {
		return value
	})	
}

// Archive a product
module.exports.archiveProduct = (reqParams, isAdmin) => {
	console.log(`Archiver is admin? ${isAdmin}`);
	let updateAvailableField = {
		isAvailable : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateAvailableField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});	
	}
	let message = Promise.resolve("Only admins can do this");
	return message.then((value) => {
		return value
	})
};

// Activate a product
module.exports.activateProduct = (reqParams, isAdmin) => {
	console.log(isAdmin);
	let updateAvailableField = {
		isAvailable : true
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateAvailableField).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});	
	}
	let message = Promise.resolve("Only admins can do this");
	return message.then((value) => {
		return value
	})
};
