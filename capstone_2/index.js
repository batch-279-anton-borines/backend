// SERVER CREATION AND DB CONNECTION
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")
const app = express();


// MongoDB Connection using SRV Link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.jfrh9pu.mongodb.net/Capstone2_API?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Optional - Validation of DB Connection
mongoose.connection.once("open", () => console.log("Connected to MongoDB Atlas"));


// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

app.use("/products", productRoutes);

app.use("/orders", orderRoutes);


// PORT LISTENING
if(require.main === module){
	app.listen(process.env.PORT || 4002, () => console.log(`API is now online on port ${process.env.PORT || 4002}`));
}

module.exports = app;