let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
	// CRUD --> R --> Retrieve
	// GET Method is somehow equal to Retrieve method in CRUD

	if(req.url == "/items" && req.method == "GET"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data retrieved from the database.");
	};

	// POST Method is used for creating/sending data to our database
	if(req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {"Content-Type" : "text/plain"});
		res.end("Data to be sent to the database.");
	};
});

server.listen(port);
console.log(`Server now running at localhost:${port}`);