// An alternative solution to the command ctrl-C is npx kill-port <port number> e.g. npx kill-port 4000
let http = require("http");

// Mock Database
let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
];

let port = 4000;
let server = http.createServer((request, response) =>{
	// Route for returning all items upon receiving a GET request
	if(request.url == `/users` && request.method == `GET`){ 
		// requests the "users" path and "get" the information

		// sets the status code to 200, denoting OK
		// --> https://www.restapitutorial.com/httpstatuscodes.html
		// sets the response output to JSON data type
		response.writeHead(200, {"Content-Type" : "application/json"});

		// input has to be data type STRING
		// This string input will be converted to desired output data type which has been set to JSON
		// This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
		response.write(JSON.stringify(directory));
		response.end();
	};

	if(request.url == `/users` && request.method == `POST`){
		// Declare and intialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on
		let requestBody = ``;

		request.on('data', (data) =>{
			// assigns the data retroeved from the data streat to request body
			requestBody += data;
		});
		request.on('end', () =>{
			// checks if at this point the requestBody is of data type STRING
			// We need this to be of data type JSON to access its properties
			console.log(typeof requestBody);

			// converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			};

			// Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		});

	}
});

server.listen(port);
console.log(`Server running at localhost:${port}`);