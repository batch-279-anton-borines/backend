const http = require("http");
let port = 4000;
let server = http.createServer((request, response)=>{
	if(request.url == `/` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Welcome to Booking System.`);
	}	

	if(request.url == `/profile` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Welcome to your Profile!`);
	}	

	if(request.url == `/courses` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Here are our available courses.`);
	}

	if(request.url == `/addcourse` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Add a course to your resources.`);
	}

	if(request.url == `/updatecourse` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Update a course to your resources.`);
	}

	if(request.url == `/archivecourse` && request.method == `GET`){
		response.writeHead(200, {"Content-Type" : "text/plain"});
		response.end(`Archive a course from your resources.`);
	}

});

server.listen(port);
console.log(`Server running at localhost:${port}`);