// [SECTION] Real World Application of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/
let trainer = {};
trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	kanto: ["Brock", "Misty"],
	hoenn: ["May", "Max"]
};
trainer.talk = function(chosen){
	return trainer.pokemon[chosen] + ", I choose you!";
}


function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 7 * level,
	this.attack = 3 * level,

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;
		if(target.health <= 0){
			console.log(target.name + "'s health is now " + target.health);
			return target.faint()
		} else {
			return target.name + "'s health is now " + target.health;
		}
	}	
	this.faint = function(){
		return this.name + " fainted.";
	}	
}

let pikachu = new Pokemon ("Pikachu", 12);
let rattatta = new Pokemon("Rattatta", 8);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);