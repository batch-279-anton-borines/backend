// 2) "s" in firstName || "d" in lastName, show only firstName, lastName, and hide _id

db.users.find({
    $or : [{
        firstName: {$regex: "s", $options: "i"}
        },
        {
        lastName: {$regex: "d", $options: "i"}
        }
    ]},
       {
           firstName: 1,
           lastName: 1,
           _id: 0
       } 
);

// 3) age >= 70; department : "HR"

db.users.find({
        $and : [
            {
                department: "HR"
            },
            {
                age: {$gte: 70}
            }
            ]
});

// 4) "e" in firstName && age <= 30

db.users.find({
    $and : [{
        firstName: {$regex : "e", $options: "i"},
        age: {$lte: 30}
    }]
});