// https://jsonplaceholder.typicode.com/todos

// No. 3 & 4

fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(res => res.json())
.then(title => {
	let titles = title.map(titleList => titleList.title);
	console.log(titles);
});

// No. 5 & 6

fetch(`https://jsonplaceholder.typicode.com/todos/14/`)
.then(res => res.json())
.then(json => console.log(json));

// No. 7

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userID: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// No. 8

fetch("https://jsonplaceholder.typicode.com/todos/14", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello Again World",
		userID: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// No. 9

fetch("https://jsonplaceholder.typicode.com/todos/14", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "More Updated Post",
		body: "Hello Once Again World",
		completed: false,
		dateCompleted: ``,
		userID: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// No. 10 & 11

fetch("https://jsonplaceholder.typicode.com/todos/14", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		title: "More Updated Post",
		body: "Hello Once Again World",
		completed: true,
		dateCompleted: `2023-05-03`,
		userID: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// No. 12

fetch(`https://jsonplaceholder.typicode.com/todos/14`, {
	method: "DELETE"
})

