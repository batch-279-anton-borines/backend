// CRUD Operation
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [SECTION] Inserting documents (Create)

// Insert One document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
	- By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@email.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

// Insert Many
/*
SYNTAX

db.collectionName.insertMany([{objectA}, objectB])
*/

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawks",
		age: 78,
		contact: {
			phone: "09123456789",
			email: "stephenhawks@mail.com"
		},
		courses: ["PHP", "React", "Python"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Arms",
		age: 82,
		contact: {
			phone: "09123456789",
			email: "neilarms@email.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	},
])

// [SECTION] Finding a single document (Retrieve)
// Leaving the search criteria empty will retrieve all documents

// SELECT * FROM users; --> query in SQL

db.users.find(); // --> query in mongdoDB

// SELECT * FROM users WHERE firstName = "Stephen"; --> query in SQL

db.users.find({ firstName: "Stephen" });

// The "pretty" method allows us to be able to view documents returned by our terminals in a better format

db.users.find({ firstName: "Stephen" }).pretty();

// Finding documents with multiple parameters

db.users.find({ lastName: "Arms"}, { age : "82"});


// [SECTION]

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "00000000000",
		email: "janedoe@email.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});

/*
SYNTAX

	db.collectionName.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
	{ firstName: "Test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "operations",
			status: "active"
		}
	}
);

// Updating Many Documents

db.users.updateMany(
	{ department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
);

// Replace One
/*
SYNTAX

	db.collectionName.replaceOne({criteria}, {$set:{field: value}});
*/

db.users.replaceOne(
	{ firstName: "Bill" },
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "09123456789",
				email: "bill@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "operations",			
		}
);

// [SECTION] Deleting Documents (Delete)

db.users.insert({
	firstName: "test"
});

// Delete Many
db.users.deleteMany({
	firstName: "test"
});

// Delete One
db.users.deleteOne({
	firstName: "test"
});


// [SECTION] Advanced queries

db.users.find({
	contact: {
		phone: "09123456789",
		email: "stephenhawks@mail.com"
	}
});

// Query on nested field

db.users.find({"contact.email": "stephenhawks@mail.com"});

// Querying an Array without regard to order

db.users.find({ courses: {$all: ["React", "Python"]}}) ; // --> more specific
db.users.find({ courses: {$all: ["React"]}}); //--> more generic

// Querying and embedding an array

db.users.insertOne({
	nameArray: [{nameA: "Juan"}, {nameB: "Tamad"}]
});	